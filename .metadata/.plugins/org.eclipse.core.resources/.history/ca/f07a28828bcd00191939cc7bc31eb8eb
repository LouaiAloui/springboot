package com.thinktank.pts.smart.mapper;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.Valid;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import com.thinktank.pts.smart.api.model.DepartementEnum;
import com.thinktank.pts.smart.api.model.StatusEnum;
import com.thinktank.pts.smart.api.model.TrainingEntry;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("deprecation")
@Entity
@Getter
@Setter
public class Training {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(updatable = false, nullable = false, name = "id")
	private String id;

	@NotBlank
	private String subject;

	@NotBlank
	private String Topic;

	@Enumerated(EnumType.STRING)
	private @Valid DepartementEnum d;

	@NotBlank
	private String date;

	@NotBlank
	private String urgency;

	@Column
	private Integer hours;

	@NotBlank
	private String description;

	@Enumerated(EnumType.STRING)
	private StatusEnum st;

	@ManyToOne()
	private User user;

	public Training() {
		super();
	}

	public Training(String id, @NotBlank String subject, @NotBlank String topic, @Valid DepartementEnum d,
			@NotBlank String date, @NotBlank String urgency, Integer hours, @NotBlank String description, StatusEnum st,
			User user) {
		super();
		this.id = id;
		this.subject = subject;
		Topic = topic;
		this.d = d;
		this.date = date;
		this.urgency = urgency;
		this.hours = hours;
		this.description = description;
		this.st = st;

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTopic() {
		return Topic;
	}

	public void setTopic(String topic) {
		Topic = topic;
	}

	public DepartementEnum getD() {
		return d;
	}

	public void setD(DepartementEnum d) {
		this.d = d;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public Integer getHours() {
		return hours;
	}

	public void setHours(Integer hours) {
		this.hours = hours;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StatusEnum getSt() {
		return st;
	}

	public void setSt(StatusEnum st) {
		this.st = st;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
	

}
