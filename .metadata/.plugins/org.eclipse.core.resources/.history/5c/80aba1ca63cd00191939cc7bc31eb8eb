package com.thinktank.pts.smart.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.thinktank.pts.smart.api.model.DepartementEnum;
import com.thinktank.pts.smart.api.model.StatusEnum;
import com.thinktank.pts.smart.api.model.UserEntry;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TrainingEntry
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-09-02T10:24:09.663+01:00[Africa/Luanda]")

public class TrainingEntry   {
  @JsonProperty("id")
  private String id;

  @JsonProperty("subject")
  private String subject;

  @JsonProperty("topic")
  private String topic;

  @JsonProperty("Departement")
  private DepartementEnum departement;

  @JsonProperty("date")
  private String date;

  @JsonProperty("urgency")
  private String urgency;

  @JsonProperty("hours")
  private Integer hours;

  @JsonProperty("description")
  private String description;

  @JsonProperty("status")
  private StatusEnum status;

  @JsonProperty("user.id")
  private UserEntry userId = null;

  public TrainingEntry id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  @ApiModelProperty(value = "")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public TrainingEntry subject(String subject) {
    this.subject = subject;
    return this;
  }

  /**
   * Get subject
   * @return subject
  */
  @ApiModelProperty(value = "")


  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public TrainingEntry topic(String topic) {
    this.topic = topic;
    return this;
  }

  /**
   * Get topic
   * @return topic
  */
  @ApiModelProperty(value = "")


  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public TrainingEntry departement(DepartementEnum departement) {
    this.departement = departement;
    return this;
  }

  /**
   * Get departement
   * @return departement
  */
  @ApiModelProperty(value = "")

  @Valid

  public DepartementEnum getDepartement() {
    return departement;
  }

  public void setDepartement(DepartementEnum departement) {
    this.departement = departement;
  }

  public TrainingEntry date(String date) {
    this.date = date;
    return this;
  }

  /**
   * Get date
   * @return date
  */
  @ApiModelProperty(value = "")


  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public TrainingEntry urgency(String urgency) {
    this.urgency = urgency;
    return this;
  }

  /**
   * Get urgency
   * @return urgency
  */
  @ApiModelProperty(value = "")


  public String getUrgency() {
    return urgency;
  }

  public void setUrgency(String urgency) {
    this.urgency = urgency;
  }

  public TrainingEntry hours(Integer hours) {
    this.hours = hours;
    return this;
  }

  /**
   * Get hours
   * @return hours
  */
  @ApiModelProperty(value = "")


  public Integer getHours() {
    return hours;
  }

  public void setHours(Integer hours) {
    this.hours = hours;
  }

  public TrainingEntry description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
  */
  @ApiModelProperty(value = "")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public TrainingEntry status(StatusEnum status) {
    this.status = status;
    return this;
  }

  /**
   * Get status
   * @return status
  */
  @ApiModelProperty(value = "")

  @Valid

  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public TrainingEntry userId(UserEntry userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   * @return userId
  */
  @ApiModelProperty(value = "")

  @Valid

  public UserEntry getUserId() {
    return userId;
  }

  public void setUserId(UserEntry userId) {
    this.userId = userId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TrainingEntry trainingEntry = (TrainingEntry) o;
    return Objects.equals(this.id, trainingEntry.id) &&
        Objects.equals(this.subject, trainingEntry.subject) &&
        Objects.equals(this.topic, trainingEntry.topic) &&
        Objects.equals(this.departement, trainingEntry.departement) &&
        Objects.equals(this.date, trainingEntry.date) &&
        Objects.equals(this.urgency, trainingEntry.urgency) &&
        Objects.equals(this.hours, trainingEntry.hours) &&
        Objects.equals(this.description, trainingEntry.description) &&
        Objects.equals(this.status, trainingEntry.status) &&
        Objects.equals(this.userId, trainingEntry.userId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, subject, topic, departement, date, urgency, hours, description, status, userId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TrainingEntry {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    subject: ").append(toIndentedString(subject)).append("\n");
    sb.append("    topic: ").append(toIndentedString(topic)).append("\n");
    sb.append("    departement: ").append(toIndentedString(departement)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    urgency: ").append(toIndentedString(urgency)).append("\n");
    sb.append("    hours: ").append(toIndentedString(hours)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

