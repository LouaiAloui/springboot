package com.thinktank.pts.smart.mapper;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.thinktank.pts.smart.api.model.DepartementEnum;
import com.thinktank.pts.smart.api.model.StatusEnum;
import com.thinktank.pts.smart.enums.*;

import lombok.Getter;
import lombok.Setter;


import java.util.Date;

import javax.persistence.*;
import javax.validation.Valid;


@SuppressWarnings("deprecation")
@Entity
@Getter
@Setter
public class SmartObject {
	
	@Id
    private String type;
	
	@Column
    private Long points;

    @NotBlank
    private String subject;

    @Enumerated(EnumType.STRING)
    private DepartementEnum d;

    @NotBlank
    private String date;

    @Column
    private Long hours;

    @NotBlank
    private String description;

    @Enumerated(EnumType.STRING)
    private Status st;

    @NotBlank
    private String urgency;


    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private String createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getPoints() {
		return points;
	}

	public void setPoints(Long integer) {
		this.points = integer;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public DepartementEnum getD() {
		return d;
	}

	public void setD(DepartementEnum departementEnum) {
		this.d = departementEnum;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Long getHours() {
		return hours;
	}

	public void setHours(Long hours) {
		this.hours = hours;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Status getSt() {
		return st;
	}

	public void setSt(StatusEnum statusEnum) {
		this.st = statusEnum;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public SmartObject(String type, Long points, @NotBlank String subject, Departement d, @NotBlank String date,
			Long hours, @NotBlank String description, Status st, @NotBlank String urgency, String createdAt,
			Date updatedAt) {
		super();
		this.type = type;
		this.points = points;
		this.subject = subject;
		this.d = getD() ;
		this.date = date;
		this.hours = hours;
		this.description = description;
		this.st = getSt();
		this.urgency = urgency;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public SmartObject() {
		super();
	}
    
    
    
}
