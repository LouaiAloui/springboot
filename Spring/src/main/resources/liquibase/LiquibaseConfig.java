import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

@Configuration
public class LiquibaseConfig {
	
	private DataSource createNewDataSource() {
		String url = "jdbc:mysql://localhost:3306/back";
		String user = "root";
		String password = "";
		return DataSourceBuilder.create().url(url).username(user).password(password).build();
	}
	
	@Bean
	public DataSource ddlDataSource() {
		JndiDataSourceLookup jndiDataSourceLookup = new JndiDataSourceLookup();
		return JndiDataSourceLookup(createNewDataSource());
	}
	@Bean
	public SpringLiquibase liquibase() {
		SpringLiquibase liquibase = new SpringLiquibase();
		liquibase.setChangeLog("classpath:/db/changelog/db.changelog-master.yaml");
		liquibase.setDataSource(ddlDataSource());
		return liquibase;
	}
}
