package com.thinktank.pts.smart.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.thinktank.pts.smart.model.SmartObject;

@Repository
public interface SmartObjectRepository extends JpaRepository<SmartObject, String>{

}
