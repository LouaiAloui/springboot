package com.thinktank.pts.smart.api.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.thinktank.pts.smart.api.SmartObjectApiDelegate;
import com.thinktank.pts.smart.api.model.SmartObjectEntry;
import com.thinktank.pts.smart.persistence.SmartObjectRepository;

@Service
public class SmartObjectApiDelegateImpl implements  SmartObjectApiDelegate{
	
	@Autowired
	SmartObjectRepository smartObjectRepository;
	
	
	@Override
	public ResponseEntity<SmartObjectEntry> addSmartObject(SmartObjectEntry smartObjectEntry) {
			
		SmartObjectEntryMapper soem = new SmartObjectEntryMapper();
		
		smartObjectRepository.save(soem.getSmartObjectEntryToSmartObject(smartObjectEntry));
		
		return ResponseEntity.ok().body(smartObjectEntry);
	}
	
	
}
