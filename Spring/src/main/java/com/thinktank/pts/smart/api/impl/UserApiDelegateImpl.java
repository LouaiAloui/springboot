package com.thinktank.pts.smart.api.impl;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.thinktank.pts.smart.api.UserApiDelegate;
import com.thinktank.pts.smart.api.model.UserEntry;
import com.thinktank.pts.smart.api.model.UserList;
import com.thinktank.pts.smart.model.User;
import com.thinktank.pts.smart.persistence.UserRepository;

@Service
public class UserApiDelegateImpl  implements UserApiDelegate{

	 @Autowired
	    UserRepository userRepository;

	@Override
	public ResponseEntity<UserEntry> addUser(UserEntry userEntry) {
		
	/*	User u = new User(userEntry);
		//userEntry.setIdu("tr");
		userRepository.save(u);  */
		
		return ResponseEntity.ok().body(userEntry);
	}

	@Override
	public ResponseEntity<List<UserList>> getUsers() {
		
		return new ResponseEntity<List<UserList>>(HttpStatus.OK);
	
	}

}
