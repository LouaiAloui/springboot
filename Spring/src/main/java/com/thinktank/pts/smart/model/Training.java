package com.thinktank.pts.smart.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;


import com.thinktank.pts.smart.api.model.DepartementEnum;
import com.thinktank.pts.smart.api.model.StatusEnum;


import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Training {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(updatable = false, nullable = false, name = "id")
	private String id;

	@NotNull
	private String subject;

	@NotNull
	private String Topic;

	@Enumerated(EnumType.STRING)
	private @Valid DepartementEnum d;

	@NotNull
	private LocalDate date;

	@NotNull
	private String urgency;

	@Column
	private Integer hours;

	@NotNull
	private String description;

	@Enumerated(EnumType.STRING)
	private StatusEnum st;

	@ManyToOne()
	private User user;

	public Training() {
		super();
	}

	public Training(String id, @NotNull String subject, @NotNull String topic, @Valid DepartementEnum d,
			@NotNull LocalDate date, @NotNull String urgency, Integer hours, @NotNull String description, StatusEnum st,
			User user) {
		super();
		this.id = id;
		this.subject = subject;
		Topic = topic;
		this.d = d;
		this.date = date;
		this.urgency = urgency;
		this.hours = hours;
		this.description = description;
		this.st = st;

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTopic() {
		return Topic;
	}

	public void setTopic(String topic) {
		Topic = topic;
	}

	public DepartementEnum getD() {
		return d;
	}

	public void setD(DepartementEnum d) {
		this.d = d;
	}

	

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public Integer getHours() {
		return hours;
	}

	public void setHours(Integer hours) {
		this.hours = hours;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StatusEnum getSt() {
		return st;
	}

	public void setSt(StatusEnum st) {
		this.st = st;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
	

}
