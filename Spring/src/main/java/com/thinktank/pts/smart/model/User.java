package com.thinktank.pts.smart.model;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;


import com.thinktank.pts.smart.api.model.UserEntry;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table(name = "user")
public class User {
		
	@Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
        name = "UUID",
    strategy = "org.hibernate.id.UUIDGenerator"
    )
	@Column(updatable = false, nullable = false, name="idu")	
	 	
	    private String idu;

	    @NotNull
	    @Column(name="username")
	    private String username;

	    @NotNull
	    @Column(name="password")
	    private String password;

	    @NotNull
	    @Column(name="firstName")
	    private String firstName;

	    @NotNull
	    @Column(name="lastName")
	    private String lastName;

	    @NotNull
	    @Column(name="roleID")
	    private String roleID;

	    @NotNull
	    @Column(name="avatar")
	    private String avatar;

	    @NotNull
	    @Column(name="email")
	    private String email;
	    
	    @OneToMany(mappedBy = "user")
	    private List<Training> trainings;
	    
	    
	    
	    public User() {
		
		}

	
		public User(String idu, @NotNull String username, @NotNull String password, @NotNull String firstName,
				@NotNull String lastName, @NotNull String roleID, @NotNull String avatar, @NotNull String email,
				List<Training> trainings) {
			super();
			this.idu = idu;
			this.username = username;
			this.password = password;
			this.firstName = firstName;
			this.lastName = lastName;
			this.roleID = roleID;
			this.avatar = avatar;
			this.email = email;
			this.trainings = trainings;
		}


		public String getIdu() {
			return idu;
		}


		public void setIdu(String idu) {
			this.idu = idu;
		}


		public String getUsername() {
			return username;
		}


		public void setUsername(String username) {
			this.username = username;
		}


		public String getPassword() {
			return password;
		}


		public void setPassword(String password) {
			this.password = password;
		}


		public String getFirstName() {
			return firstName;
		}


		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}


		public String getLastName() {
			return lastName;
		}


		public void setLastName(String lastName) {
			this.lastName = lastName;
		}


		public String getRoleID() {
			return roleID;
		}


		public void setRoleID(String roleID) {
			this.roleID = roleID;
		}


		public String getAvatar() {
			return avatar;
		}


		public void setAvatar(String avatar) {
			this.avatar = avatar;
		}


		public String getEmail() {
			return email;
		}


		public void setEmail(String email) {
			this.email = email;
		}


		public List<Training> getTrainings() {
			return trainings;
		}


		public void setTrainings(List<Training> trainings) {
			this.trainings = trainings;
		}

	
		
		
	    
}
