package com.thinktank.pts.smart.model;



import com.thinktank.pts.smart.api.model.DepartementEnum;
import com.thinktank.pts.smart.api.model.StatusEnum;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;


import javax.persistence.*;

import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;



@Entity
@Getter
@Setter
public class SmartObject {
	
	@Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
        name = "UUID",
    strategy = "org.hibernate.id.UUIDGenerator"
    )
	@Column(updatable = false, nullable = false, name="type")
    private String type;
	
	@Column
    private int points;

    @NotNull
    private String subject;

    @Enumerated(EnumType.STRING)
    private DepartementEnum d;

    
    private LocalDate date;

    @Column
    private int hours;

    @NotNull
    private String description;

    @Enumerated(EnumType.STRING)
    private StatusEnum st;

    @Column
    private String urgency;


    @Column(nullable = false, updatable = false)
    private LocalDate createdAt;

    @Column(nullable = false)
    private LocalDate updatedAt;

	

	public SmartObject() {
		super();
	}

	public SmartObject(String type, int points, @NotNull String subject, DepartementEnum d,  LocalDate date,
			int hours, @NotNull String description, StatusEnum st,  String urgency, LocalDate createdAt,
			LocalDate updatedAt) {
		super();
		
		this.type = type;
		this.points = points;
		this.subject = subject;
		this.d = d;
		this.date = date;
		this.hours = hours;
		this.description = description;
		this.st = st;
		this.urgency = urgency;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}
	



	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public DepartementEnum getD() {
		return d;
	}

	public void setD(DepartementEnum d) {
		this.d = d;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StatusEnum getSt() {
		return st;
	}

	public void setSt(StatusEnum st) {
		this.st = st;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public LocalDate getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDate createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDate getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDate updatedAt) {
		this.updatedAt = updatedAt;
	}

	
    


	
    
    
    
}
