package com.thinktank.pts.smart.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class SmartActionType {
	
	 @Id
	 private String idSAT;

	 @NotBlank
	 private String name;
	 
	 @Column
	 private Long number;
	    
}
