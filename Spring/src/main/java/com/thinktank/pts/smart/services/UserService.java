package com.thinktank.pts.smart.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.thinktank.pts.smart.model.User;
import com.thinktank.pts.smart.persistence.UserRepository;

public class UserService {

	@Autowired
	UserRepository userRepository;

	public List<User> getAllUser() {
		return userRepository.findAll();
	}

}
