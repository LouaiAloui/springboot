package com.thinktank.pts.smart.api.impl;

import com.thinktank.pts.smart.api.model.TrainingEntry;
import com.thinktank.pts.smart.model.Training;

public class TrainingEntryMapper {
	

	public Training getEntryTrainingToTraining( TrainingEntry trainingEntry) {
		
		Training tr = new Training();
		
		tr.setId(trainingEntry.getId());
		tr.setD(trainingEntry.getDepartement());
		tr.setDate(trainingEntry.getDate());
		tr.setDescription(trainingEntry.getDescription());
		tr.setHours(trainingEntry.getHours());
		tr.setSt(trainingEntry.getStatus());
		tr.setSubject(trainingEntry.getSubject());
		tr.setTopic(trainingEntry.getTopic());
		tr.setUrgency(trainingEntry.getUrgency());
		
		return tr;
	}
	

}
