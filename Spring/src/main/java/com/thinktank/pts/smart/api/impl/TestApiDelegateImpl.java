package com.thinktank.pts.smart.api.impl;



import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.thinktank.pts.smart.api.TestApiDelegate;

@Service
public class TestApiDelegateImpl implements TestApiDelegate{

	@Override
	public ResponseEntity<String> test() {
		// TODO Auto-generated method stub
		return ResponseEntity.ok().body("Hello World!");
	}
	
	

	
}
